﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace esoft
{
    public partial class CoeffControl : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial Catalog=esoft2;Integrated Security=true;");
        public int managerId = 0;

        public CoeffControl()
        {
            InitializeComponent();
        }

        private void CoeffControl_Load(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("SELECT Junior, Middle, Senior, Коэффициент_для_Анализ_и_проектирование, Коэффициент_для_Установка_оборудования, Коэффициент_для_Техническое_обслуживание_и_сопровождение, Коэффициент_сложности, Коэффициент_времени, Коэффициент_для_перевода_в_денежный_эквивалент " +
                                            "FROM coeffs " +
                                            $"WHERE id = '{managerId}'", con);
            SqlDataReader dr = com.ExecuteReader();
            dr.Read();
            juniorText.Text = dr[0].ToString();
            middleText.Text = dr[1].ToString();
            seniorText.Text = dr[2].ToString();
            workCoeff1.Text = dr[3].ToString();
            workCoeff2.Text = dr[4].ToString();
            workCoeff3.Text = dr[5].ToString();
            diffCoeff.Text = dr[6].ToString();
            timeCoeff.Text = dr[7].ToString();
            transfCoeff.Text = dr[8].ToString();
            dr.Close();

            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            errors.Text = "";
            int junior = 0;
            int middle = 0;
            int senior = 0;
            double wc1 = 0;
            double wc2 = 0;
            double wc3 = 0;
            double diffC = 0;
            double timeC = 0;
            double transfC = 0;

            //Проверка коэффициентов гарантированного минимума зарплаты
            if (Regex.IsMatch(juniorText.Text, @"[^\d]+")) errors.Text += "Используйте только положительные числа для заполнения поля минимальной оплаты.\n";
            else junior = Convert.ToInt32(juniorText.Text);
            if (Regex.IsMatch(middleText.Text, @"[^\d]+")) errors.Text += "Используйте только положительные числа для заполнения поля минимальной оплаты.\n";
            else middle = Convert.ToInt32(middleText.Text);
            if (Regex.IsMatch(seniorText.Text, @"[^\d]+")) errors.Text += "Используйте только положительные числа для заполнения поля минимальной оплаты.\n";
            else senior = Convert.ToInt32(seniorText.Text);

            //Проверка коэффициента "Анализ и проектировка"
            try
            {
                workCoeff1.Text = Regex.Replace(workCoeff1.Text, @"\.", ",");
                if (Convert.ToDouble(workCoeff1.Text) >= 0 && Convert.ToDouble(workCoeff1.Text) <= 1) wc1 = Convert.ToDouble(workCoeff1.Text);
                else errors.Text += "Диапазон для поля коэффициента \"Анализ и проектировка\" должен быть от 0 до 1";
            }
            catch (FormatException)
            {
                errors.Text += "Ошибка в поле \"Анализ и проектировка\"\n";
            }

            //Проверка коэффициента "Установка оборудования"
            try
            {
                workCoeff2.Text = Regex.Replace(workCoeff2.Text, @"\.", ",");
                if (Convert.ToDouble(workCoeff2.Text) >= 0 && Convert.ToDouble(workCoeff2.Text) <= 1) wc2 = Convert.ToDouble(workCoeff2.Text);
                else errors.Text += "Диапазон для поля коэффициента \"Установка оборудования\" должен быть от 0 до 1";
            }
            catch
            {
                errors.Text += "Ошибка в поле \"Установка оборудования\"\n";
            }

            //Проверка коэффициента "Техническое обслуживание и сопровождение"
            try
            {
                workCoeff3.Text = Regex.Replace(workCoeff3.Text, @"\.", ",");
                if (Convert.ToDouble(workCoeff3.Text) >= 0 && Convert.ToDouble(workCoeff3.Text) <= 1) wc3 = Convert.ToDouble(workCoeff3.Text);
                else errors.Text += "Диапазон для поля коэффициента \"Техническое обслуживание и сопровождение\" должен быть от 0 до 1";
            }
            catch
            {
                errors.Text += "Ошибка в поле \"Техническое обслуживание и сопровождение\"\n";
            }

            //Проверка коэффициента сложности
            try
            {
                diffCoeff.Text = Regex.Replace(diffCoeff.Text, @"\.", ",");
                diffC = Convert.ToDouble(diffCoeff.Text);
            }
            catch
            {
                errors.Text += "Ошибка в поле \"Коэффициент сложности\"\n";
            }

            //Проверка коэффициента времени
            try
            {
                timeCoeff.Text = Regex.Replace(timeCoeff.Text, @"\.", ",");
                timeC = Convert.ToDouble(timeCoeff.Text);
            }
            catch
            {
                errors.Text += "Ошибка в поле \"Коэффициент времени\"\n";
            }

            //Проверка коэффициента перевода в денежный эквивалент
            try
            {
                transfCoeff.Text = Regex.Replace(transfCoeff.Text, @"\.", ",");
                transfC = Convert.ToDouble(transfCoeff.Text);
            }
            catch
            {
                errors.Text += "Ошибка в поле \"Коэффициент перевода в денежный эквивалент\"\n";
            }

            con.Open();
            if (errors.Text.Length == 0)
            {
                SqlCommand com = new SqlCommand($"UPDATE coeffs SET " +
                                                $"[Коэффициент_для_Анализ_и_проектирование] = {Regex.Replace(wc1.ToString(), ",", ".")}, " +
                                                $"[Коэффициент_для_Установка_оборудования] = {Regex.Replace(wc2.ToString(), ",", ".")}, " +
                                                $"[Коэффициент_для_Техническое_обслуживание_и_сопровождение] = {Regex.Replace(wc3.ToString(), ",", ".")}, " +
                                                $"[Коэффициент_времени] = {Regex.Replace(timeC.ToString(), ",", ".")}, " +
                                                $"[Коэффициент_сложности] = {Regex.Replace(diffC.ToString(), ",", ".")}, " +
                                                $"[Коэффициент_для_перевода_в_денежный_эквивалент] = {Regex.Replace(transfC.ToString(), ",", ".")}, " +
                                                $"[Junior] = {Regex.Replace(junior.ToString(), ",", ".")}, " +
                                                $"[Middle] = {Regex.Replace(middle.ToString(), ",", ".")}, " +
                                                $"[Senior] = {Regex.Replace(senior.ToString(), ",", ".")} " +
                                                $"WHERE id = '{managerId}'", con);
                if (com.ExecuteNonQuery() != 0) MessageBox.Show("Коэффициенты успешно изменены.");
                else MessageBox.Show("Произошла ошибка при добавлении");
            }

            con.Close();
        }

        private void errors_Click(object sender, EventArgs e)
        {

        }
    }
}
