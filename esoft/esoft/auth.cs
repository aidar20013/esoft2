﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace esoft
{
    public partial class Auth : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial Catalog=esoft2; Integrated Security=true;");

        public Auth()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con.Open();

            string login = loginText.Text;
            string pass = passText.Text;
            int userId = 0;
            int managerId = 0;

            SqlCommand usersLoginCom = new SqlCommand($"SELECT id FROM users WHERE login = '{login}' AND password = '{pass}'", con);
            SqlCommand managersLoginCom = new SqlCommand($"SELECT id FROM managers WHERE login = '{login}' AND pass = '{pass}'", con);

            SqlDataReader drManagers = managersLoginCom.ExecuteReader();
            if (drManagers.HasRows)
            {
                drManagers.Read();
                Menu menuForm = new Menu();
                managerId = Convert.ToInt32(drManagers[0]);
                menuForm.managerId = managerId;
                menuForm.Show();
            }
            else
            {
                drManagers.Close();
                SqlDataReader drUsers = usersLoginCom.ExecuteReader();
                if (drUsers.HasRows)
                {
                    drUsers.Read();
                    Tasks tasksForm = new Tasks();
                    userId = Convert.ToInt32(drUsers[0]);
                    tasksForm.userId = userId;
                    tasksForm.Show();
                    drUsers.Close();
                }
            }

            if (managerId == 0 && userId == 0) MessageBox.Show("Неверный логин или пароль.");
            
            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
