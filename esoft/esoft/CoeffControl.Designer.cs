﻿namespace esoft
{
    partial class CoeffControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.juniorText = new System.Windows.Forms.TextBox();
            this.middleText = new System.Windows.Forms.TextBox();
            this.seniorText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.workCoeff1 = new System.Windows.Forms.TextBox();
            this.workCoeff2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.workCoeff3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.diffCoeff = new System.Windows.Forms.TextBox();
            this.timeCoeff = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.transfCoeff = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.errors = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // juniorText
            // 
            this.juniorText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.juniorText.Location = new System.Drawing.Point(108, 115);
            this.juniorText.Name = "juniorText";
            this.juniorText.Size = new System.Drawing.Size(123, 22);
            this.juniorText.TabIndex = 0;
            // 
            // middleText
            // 
            this.middleText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.middleText.Location = new System.Drawing.Point(108, 159);
            this.middleText.Name = "middleText";
            this.middleText.Size = new System.Drawing.Size(123, 22);
            this.middleText.TabIndex = 1;
            // 
            // seniorText
            // 
            this.seniorText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.seniorText.Location = new System.Drawing.Point(108, 205);
            this.seniorText.Name = "seniorText";
            this.seniorText.Size = new System.Drawing.Size(123, 22);
            this.seniorText.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(46, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "Junior";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(46, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Middle";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(46, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 14);
            this.label3.TabIndex = 5;
            this.label3.Text = "Senior";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(44, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(324, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "Коэффициенты гарантированного минимума зарплаты:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.Location = new System.Drawing.Point(48, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(279, 14);
            this.label5.TabIndex = 7;
            this.label5.Text = "Коэффициенты характера выполненных работ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label6.Location = new System.Drawing.Point(47, 300);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 14);
            this.label6.TabIndex = 8;
            this.label6.Text = "Анализ и проектирование";
            // 
            // workCoeff1
            // 
            this.workCoeff1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.workCoeff1.Location = new System.Drawing.Point(329, 300);
            this.workCoeff1.Name = "workCoeff1";
            this.workCoeff1.Size = new System.Drawing.Size(100, 22);
            this.workCoeff1.TabIndex = 9;
            // 
            // workCoeff2
            // 
            this.workCoeff2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.workCoeff2.Location = new System.Drawing.Point(329, 343);
            this.workCoeff2.Name = "workCoeff2";
            this.workCoeff2.Size = new System.Drawing.Size(100, 22);
            this.workCoeff2.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label7.Location = new System.Drawing.Point(47, 346);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 14);
            this.label7.TabIndex = 10;
            this.label7.Text = "Установка оборудования";
            // 
            // workCoeff3
            // 
            this.workCoeff3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.workCoeff3.Location = new System.Drawing.Point(329, 391);
            this.workCoeff3.Name = "workCoeff3";
            this.workCoeff3.Size = new System.Drawing.Size(100, 22);
            this.workCoeff3.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label8.Location = new System.Drawing.Point(45, 394);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(276, 14);
            this.label8.TabIndex = 12;
            this.label8.Text = "Техническое обслуживание и сопровождение";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label9.Location = new System.Drawing.Point(640, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 14);
            this.label9.TabIndex = 14;
            this.label9.Text = "Коэффициент сложности";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label10.Location = new System.Drawing.Point(654, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 14);
            this.label10.TabIndex = 15;
            this.label10.Text = "Коэффициент времени";
            // 
            // diffCoeff
            // 
            this.diffCoeff.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.diffCoeff.Location = new System.Drawing.Point(826, 118);
            this.diffCoeff.Name = "diffCoeff";
            this.diffCoeff.Size = new System.Drawing.Size(138, 22);
            this.diffCoeff.TabIndex = 16;
            // 
            // timeCoeff
            // 
            this.timeCoeff.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.timeCoeff.Location = new System.Drawing.Point(826, 161);
            this.timeCoeff.Name = "timeCoeff";
            this.timeCoeff.Size = new System.Drawing.Size(138, 22);
            this.timeCoeff.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label11.Location = new System.Drawing.Point(482, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(313, 14);
            this.label11.TabIndex = 18;
            this.label11.Text = "Коэффициент для перевода в денежный эквивалент";
            // 
            // transfCoeff
            // 
            this.transfCoeff.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.transfCoeff.Location = new System.Drawing.Point(826, 208);
            this.transfCoeff.Name = "transfCoeff";
            this.transfCoeff.Size = new System.Drawing.Size(138, 22);
            this.transfCoeff.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button1.Location = new System.Drawing.Point(48, 481);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Изменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button2.Location = new System.Drawing.Point(890, 481);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Выход";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(238, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "руб.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(238, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "руб.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(238, 217);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "руб.";
            // 
            // errors
            // 
            this.errors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.errors.Location = new System.Drawing.Point(487, 253);
            this.errors.Name = "errors";
            this.errors.Size = new System.Drawing.Size(477, 216);
            this.errors.TabIndex = 25;
            this.errors.Click += new System.EventHandler(this.errors_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::esoft.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(421, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // CoeffControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 529);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.errors);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.transfCoeff);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.timeCoeff);
            this.Controls.Add(this.diffCoeff);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.workCoeff3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.workCoeff2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.workCoeff1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.seniorText);
            this.Controls.Add(this.middleText);
            this.Controls.Add(this.juniorText);
            this.Name = "CoeffControl";
            this.Text = "CoeffControl";
            this.Load += new System.EventHandler(this.CoeffControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox juniorText;
        private System.Windows.Forms.TextBox middleText;
        private System.Windows.Forms.TextBox seniorText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox workCoeff1;
        private System.Windows.Forms.TextBox workCoeff2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox workCoeff3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox diffCoeff;
        private System.Windows.Forms.TextBox timeCoeff;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox transfCoeff;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label errors;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}