﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace esoft
{
    public partial class NewTask : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial Catalog=esoft2;Integrated Security=true;");
        public int managerId;

        public NewTask()
        {
            InitializeComponent();
        }

        private void NewTask_Load(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand($"SELECT name FROM users WHERE manager_id = {managerId}", con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
                executorText.Items.Add(dr[0].ToString());
            con.Close();

            finishDate.MinDate = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con.Open();

            string errors = "";
            string title = "";
            string desc = "";
            string executor = "";
            string status = "";
            string taskType = "";
            string executorLogin = "";
            int diff = 0;
            int time = 0;
            DateTime fDate = new DateTime();
            
            fDate = finishDate.Value;


            if (taskTypeText.Text.Length > 0) taskType = taskTypeText.Text;
            else errors += "Укажите вид работ.\n";
            
            if (statusText.Text.Length > 0) status = statusText.Text;
            else errors += "Укажите статус задачи.\n";

            if (executorText.Text.Length >= 0) executor = executorText.Text;
            else errors += "Выберите исполнителя задачи.\n";
            
            if (titleText.Text.Length > 0) title = titleText.Text;
            else errors += "Укажите название задачи.\n";

            if (descText.Text.Length > 0) desc = descText.Text;
            else errors += "Укажите описание задачи.\n";

            if (Regex.IsMatch(diffText.Text, @"[^\d]") || diffText.Text.Length == 0) errors += "Используйте только цифры от 1 до 50 для выражения сложности задачи.\n";
            else
            {
                if (Convert.ToInt16(diffText.Text) >= 1 && Convert.ToInt16(diffText.Text) <= 50)
                {
                    diff = Convert.ToInt16(diffText.Text);
                }
                else errors += "Используйте только цифры от 1 до 50 для выражения сложности задачи.\n";
            }

            if (Regex.IsMatch(timeText.Text, @"[^\d]") || timeText.Text.Length == 0) errors += "Используйте только цифры для заполнения времени выполнения задачи.\n";
            else time = Convert.ToInt16(timeText.Text);

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                errors = "";
            }
            else
            {
                SqlCommand com = new SqlCommand($"SELECT login FROM users WHERE name = '{executor}'", con);
                SqlDataReader dr = com.ExecuteReader();
                dr.Read();
                executorLogin = dr[0].ToString();
                dr.Close();

                com = new SqlCommand("INSERT INTO tasks_for_executors(login, title, text, difficulty, status, work_type, end_time, time_to_work, created_time) " +
                                    $"VALUES ('{executorLogin}', '{title}', '{desc}', '{diff}', '{status}', '{taskType}', '{fDate}', '{time}', '{DateTime.Now}')", con);
                if (com.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Задача успешно добавлена.");
                    Tasks tasksForm = (Tasks)Application.OpenForms["Tasks"];

                }
                else MessageBox.Show("Произошла ошибка.");
            }

            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
