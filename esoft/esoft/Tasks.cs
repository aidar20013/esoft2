﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace esoft
{
    public partial class Tasks : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial Catalog=esoft2;Integrated Security=true;");
        public int userId = 0;
        public int managerId = 0;
        string executor = "";
        string status = "";


        public Tasks()
        {
            InitializeComponent();
        }

        private void Tasks_Load(object sender, EventArgs e)
        {
            con.Open();
            if (userId != 0)
            {

                SqlCommand com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                                $"FROM tasks_for_executors, users, managers " +
                                                $"WHERE users.login = tasks_for_executors.login " +
                                                $"AND users.id = {userId} " +
                                                $"AND managers.id = users.manager_id " +
                                                $"AND tasks_for_executors.deleted = '0'", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
                dr.Close();
            }

            if (managerId != 0)
            {
                label2.Visible = true;
                comboBox2.Visible = true;
                button3.Visible = true;
                SqlCommand com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                                $"FROM tasks_for_executors, users, managers " +
                                                $"WHERE tasks_for_executors.login = users.login " +
                                                $"AND users.manager_id = {managerId} " +
                                                $"AND managers.id = {managerId} " +
                                                $"AND tasks_for_executors.deleted = '0'", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
                    if (!(comboBox2.Items.IndexOf(dr[2].ToString()) >= 0)) comboBox2.Items.Add(dr[2].ToString());
                }
                dr.Close();
                    
            }
            con.Close();
        }

        //Фильтр по статусу задачи
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();

            SqlCommand com = new SqlCommand();
            status = comboBox1.SelectedItem.ToString();
            if (userId != 0)
            {
                if (executor.Length != 0)
                {
                    com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                         $"FROM tasks_for_executors, users, managers " +
                                         $"WHERE users.login = tasks_for_executors.login " +
                                         $"AND users.id = {userId} " +
                                         $"AND managers.id = users.manager_id " +
                                         $"AND tasks_for_executors.status = '{status}' " +
                                         $"AND users.name = '{executor}' " +
                                         $"AND tasks_for_executors.deleted = '0'", con);


                }
                else
                {
                    com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                         $"FROM tasks_for_executors, users, managers " +
                                         $"WHERE users.login = tasks_for_executors.login " +
                                         $"AND users.id = {userId} " +
                                         $"AND managers.id = users.manager_id " +
                                         $"AND tasks_for_executors.status = '{status}' " +
                                         $"AND tasks_for_executors.deleted = '0'", con);
                }
            }
            if (managerId != 0)
            {
                if (executor.Length != 0)
                {
                    com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                         $"FROM tasks_for_executors, users, managers " +
                                         $"WHERE tasks_for_executors.login = users.login " +
                                         $"AND users.manager_id = {managerId} " +
                                         $"AND managers.id = {managerId} " +
                                         $"AND users.name = '{executor}' " +
                                         $"AND tasks_for_executors.status = '{status}' " +
                                         $"AND tasks_for_executors.deleted = '0'", con);
                }
                else
                {
                    com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                         $"FROM tasks_for_executors, users, managers " +
                                         $"WHERE tasks_for_executors.login = users.login " +
                                         $"AND users.manager_id = {managerId} " +
                                         $"AND managers.id = {managerId} " +
                                         $"AND tasks_for_executors.status = '{status}' " +
                                         $"AND tasks_for_executors.deleted = '0'", con);
                }
            }
            SqlDataReader dr = com.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (dr.Read())
            {
                dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
            }
            dr.Close();
            con.Close();
        }

        //Фильтр по исполнителю
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand();
            executor = comboBox2.SelectedItem.ToString();
            if (status.Length != 0)
            {
                com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                     $"FROM tasks_for_executors, users, managers " +
                                     $"WHERE tasks_for_executors.login = users.login AND users.manager_id = {managerId} " +
                                     $"AND managers.id = {managerId} " +
                                     $"AND users.name = '{executor}' " +
                                     $"AND tasks_for_executors.status = '{status}' " +
                                     $"AND tasks_for_executors.deleted = '0'", con);
            }
            else
            {
                com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                     $"FROM tasks_for_executors, users, managers " +
                                     $"WHERE tasks_for_executors.login = users.login " +
                                     $"AND users.manager_id = {managerId} " +
                                     $"AND managers.id = {managerId} " +
                                     $"AND users.name = '{executor}' " +
                                     $"AND tasks_for_executors.deleted = '0'", con);

            }
            SqlDataReader dr = com.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (dr.Read())
            {
                dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
            }
            dr.Close();
            con.Close();
        }

        //Сброс фильтров
        private void button1_Click(object sender, EventArgs e)
        {
            con.Open();
            status = "";
            executor = "";
            SqlCommand com = new SqlCommand();
            if (userId != 0)
            {
                com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                     $"FROM tasks_for_executors, users, managers " +
                                     $"WHERE users.login = tasks_for_executors.login " +
                                     $"AND users.id = {userId} " +
                                     $"AND managers.id = users.manager_id " +
                                     $"AND tasks_for_executors.deleted = '0'", con);

            }
            if (managerId != 0)
            {
                com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                     $"FROM tasks_for_executors, users, managers " +
                                     $"WHERE tasks_for_executors.login = users.login " +
                                     $"AND users.manager_id = {managerId} " +
                                     $"AND managers.id = {managerId} " +
                                     $"AND tasks_for_executors.deleted = '0'", con);
            }
            SqlDataReader dr = com.ExecuteReader();
            dataGridView1.Rows.Clear();
            while (dr.Read())
                dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
            dr.Close();
            con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NewTask newTaskForm = new NewTask();
            newTaskForm.managerId = managerId;
            newTaskForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int row = dataGridView1.CurrentRow.Index;
            /*ModifieTask mtForm = new ModifieTask();
            mtForm.userId = userId;
            mtForm.managerId = managerId;
            mtForm.Show();*/
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int currentRow = dataGridView1.CurrentRow.Index;
            string title = dataGridView1.Rows[currentRow].Cells[0].Value.ToString();
            con.Open();

            DialogResult test = MessageBox.Show("Вы действительно хотите удалить эту задачу?", "Подтвердите удаление", MessageBoxButtons.OKCancel);
            if (test.ToString() == "OK")
            {
                SqlCommand com = new SqlCommand($"UPDATE tasks_for_executors SET deleted = '1' WHERE title = '{title}'", con);
                com.ExecuteNonQuery();
                dataGridView1.Rows.Clear();
                if (userId != 0)
                {

                    com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                         $"FROM tasks_for_executors, users, managers " +
                                         $"WHERE users.login = tasks_for_executors.login " +
                                         $"AND users.id = {userId} " +
                                         $"AND managers.id = users.manager_id " +
                                         $"AND tasks_for_executors.deleted = '0'", con);
                    SqlDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
                    dr.Close();
                }

                if (managerId != 0)
                {
                    com = new SqlCommand($"SELECT title, status, users.name, managers.name " +
                                         $"FROM tasks_for_executors, users, managers " +
                                         $"WHERE tasks_for_executors.login = users.login " +
                                         $"AND users.manager_id = {managerId} " +
                                         $"AND managers.id = {managerId} " +
                                         $"AND tasks_for_executors.deleted = '0'", con);
                    SqlDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
                    dr.Close();
                }
            }

            con.Close();
        }
    }
}
